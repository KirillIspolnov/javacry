package org.kllbff.jcry;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Launcher {
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print("Введите путь к файлу с исходным кодом: ");
        File source = new File(scanner.next());
        
        long start = System.currentTimeMillis();
        JavaCry cry = new JavaCry();
        
        try {
            cry.setSourceFile(source);
            cry.removeComments();
            cry.removeWhitespaces();
            cry.renameClass("AvadaKedabra");
            cry.renameVariables();
            
        } catch(IOException e) {
            System.out.println("При работе с исходным кодом произошла ошибка: " + e);
        }
        
        try {
            FileWriter writer = new FileWriter(source.getAbsoluteFile().getParent() + "/AvadaKedabra.java");
            writer.write(cry.toString());
            writer.flush();
            writer.close();
        } catch(IOException e) {
            System.out.println("Во время записи обфусцировнаного кода произошла ошибка: " + e);
        }
        System.out.printf("Время работы: %.3f сек.\n", (System.currentTimeMillis() - start) / 1000f);
    }

}
