package org.kllbff.jcry;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaCry {
    /**
     * Массив 50 ключевых cлов Java SE 8
     */
    public static final String[] KEYWORDS = {
        "byte", "short", "int", "long", "char", "float", "double", "boolean",
        "if", "else", "switch", "case", "default", "while", "do", "break", "continue", "for",
        "try", "catch", "finally", "throw", "throws", 
        "private", "protected", "public",
        "import", "package", "class", "interface", "extends", "implements", "static", "final", "void", "abstract", "native",
        "new", "return", "this", "super",
        "synchronized", "volatile",
        "const", "goto", "enum", "instanceof", "assert", "transient", "strictfp"
    };
    public static final String METASYMBOLS_REGEX = "[>\\]]";
    public static final String ALL_KEYWORDS_REGEX;

    static {
        StringJoiner joiner = new StringJoiner("|");
        for(String keyword : KEYWORDS) {
            joiner.add(keyword);
        }
        ALL_KEYWORDS_REGEX = joiner.toString();
    }
    
    private Pattern WHITESPACES_PATTERN = Pattern.compile(" +");
    private Pattern OPERATORS_PATTERN = Pattern.compile("\\s*[:,;=/*+\\-{}()?]+\\s*");
    private Pattern VARIABLE_DEFINITION_PATTERN = Pattern.compile("(?<= )[_a-zA-Z](\\w*)(?=[,;:=])");
    private Pattern STRING_LITERAL_ID_PATTERN = Pattern.compile("%\\d+");
    
    /**
     * Исходный код 
     */
    private ArrayList<String> lines = new ArrayList<String>();
    private ArrayList<String> stringLiterals = new ArrayList<String>();
    
    /**
     * Задает объекту новый исходный код для его дальнейшей обработки
     * 
     * @param reader 
     */
    public void setSource(Reader reader) throws IOException {
        try(BufferedReader breader = new BufferedReader(reader)) {
            String line;
            while((line = breader.readLine()) != null) {
                addSourceLine(line);
            }
        }
    }

    /**
     * Задает объекту новый исходный код для его дальнейшей обработки
     * 
     * @param source исходный код
     */
    public void setSource(CharSequence source) {
        String[] lines = source.toString().split("\n");
        for(String line : lines) {
            addSourceLine(line);
        }
    }
    
    /**
     * Задает объекту исходный код для его дальнейшей обработки
     * 
     * @param file файл с исходным кодом
     * @throws IOException в случае возникновения ошибок ввода-вывод
     */
    public void setSourceFile(File file) throws IOException {
        try(FileReader reader = new FileReader(file)) {
            this.setSource(reader);
        }
    }
      
    /**
     * Добавляет очередную строчку в список строк исходного кода
     *  
     * @param line строка кода
     */
    private void addSourceLine(String line) {
        if(!line.isEmpty()) {
            line = line.replace("\\", "\\\\");
            line = saveLiterals(line, '"');
            line = saveLiterals(line, '\'');
            this.lines.add(line);   
        }
    }
    
    /**
     * Сохраняет найденные в строке литералы и подменяет их вхождения
     * на специальный код, связывающий исходное содержимое литерала
     * и место его вхождения
     * 
     * @param line исходная строка
     * @param border символ, обозначающий границы литерала
     * @return строка, в которой все литералы подменены на коды
     */
    private String saveLiterals(String line, char border) {
        //индекс открывающей кавычки
        int literalStart;
        //перебор всех строковых литералов в строке  
        while((literalStart = line.indexOf(border)) != -1) {
            //поиск закрывающей кавычки
            int literalEnd = line.indexOf(border, literalStart + 1);
            
            //закрывающая кавычка не найдена
            if(literalEnd == -1)
                break;
            
            //игнорирование экранированных кавычек
            while(line.charAt(literalEnd - 1) == '\\' && literalEnd != line.lastIndexOf(border)) {
                literalEnd = line.indexOf(border, literalEnd + 1);
            }
            
            //выделение литерала с кавычками
            String literal = line.substring(literalStart, literalEnd + 1);
            //замена на код вида %<индекс литерала в списке>
            line = line.replace(literal, "%" + stringLiterals.size());
            //сохранение литерала в списке
            stringLiterals.add(literal);
        }
        return line;
    }
    
    /**
     * Удаляет много- и однострочные комментарии
     * 
     * В данных случаях:
     * 1. <code>
     *  String aaa = bbb.substring(0, 2); //comment
     * </code>
     * 2. <code>
     *  String aaa = bbb.substring(0, 2); /* comment
     *  new line of comment *\ 
     * </code>
     * исходный код будет сохранен, а комментарий успешно удален
     */
    public void removeComments() {
        
        boolean commentOpened = false;
        for(int i = 0; i < lines.size(); /* переключается костылями */) {
            String line = lines.get(i);
            
            /* строка содержит конец комментария */
            if(commentOpened && line.contains("*/")) {
                //отключаем флаг
                commentOpened = false;
                //удаляем комментарий из строки
                //после символа */ модет идти код, который необходимо
                //сохранить в исходном виде
                line = line.replaceAll(".*\\*/", "");
            }
            
            if(line.contains("//")) {
                //однострочный комментарий
                line = line.replaceAll("//.*", "");
            }
            
            if(commentOpened) {
                /* строка есть часть многострочного комментария 
                 * долой ее
                 */
                lines.remove(i);
                //мы удалили один элемент из списка
                //и все оставшиеся строки в нем сдвинулись влево
                //поэтому значение параметра i менять не нужно
                continue;
            }

            if(line.contains("/*")) {
                //если строка не содержит символов */, т.е не содержит
                //конец комментария, включаем флаг
                commentOpened = !line.contains("*/");
                
                if(commentOpened) {
                    line = line.replaceAll("/\\*.*", "");
                } else {
                    line = line.replaceAll("/\\*.*\\*/", "");
                }
            }
            
            //обновляем строку в списке с учетом внесенных изменений
            lines.set(i, line);
            //переходим к следующей строке, увеличивая счетчик на 1
            i++;
        }
    }
    
    /**
     * Изменяет имя класса
     * Меняет имя класса в его сигнатуре, а также
     * переименовывает все конструкторы
     * 
     * Вводный курс по теме "Поиск имени класса":
     * <ol>
     *  <li>Ищем строчку с сигнатурой класса</li>
     *  <li>Если строка содержит символ {, то удаляем 
     *      его и всё, что следует за ним<br>
     *      Иначе убираем все пробелы в конце подстроки</li>
     *  <li>Из найденной строки отрезаем кусочек
     *      после ключевого слова class</li>
     *  <li>Радуемся</li>
     * </ol>
     * 
     * @param newName новое имя класса
     */
    public void renameClass(String newName) {
        //сигнатура класса
        String signature = null;
        
        for(String line : lines) {
            /*
             * #0 Если строка начинается с ключевых слов, 
             * используемых в записи сигнатуры класса,
             * то эта строка и есть сигнатура класса
             */
            if(line.matches("(public |final |abstract |)class .*")) {
                signature = line;
                break;
            }
        }
        
        /*
         * #1 Выбираем до какого символа находится то,
         * что нам нужно  
         */
        int index = signature.indexOf("{");
        if(index == -1) {
            /*
             * Символ { не найден
             * Так что сохраним всё, шо есть
             */
            index = signature.length();
        }
        
        /*
         * #2 Достаем из строки всё, что идет после слова class
         * до определенного выше символа (позиция символа { или конец строки)
         */
        signature = signature.substring(signature.indexOf("class ") + 6, index);
        
        /*
         * Долой пробелы
         */
        String name = signature.trim();
        
        /*
         * Меняем все совпадения с именем, шо есть в коде!
         */
        for(int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            /*
             * сигнатура класса, имена конструкторов
             */
            line = line.replaceAll("(?<= )" + name + "(?= ?[({])", newName);
            /* 
             * конструкции вида <имя класса>.this
             */
            line = line.replaceAll(name + "(?=\\.this)", newName);
            lines.set(i, line);
        }
    }
    
    /**
     * Удаляет неиспользуемые пробелы и пустые строки
     */
    public void removeWhitespaces() {
        Matcher matcher;
        for(int i = 0; i < lines.size(); /* снова костыли */) {
            String line = lines.get(i);
            
            /* удаляем пустые строки */
            if(line.trim().isEmpty()) {
                lines.remove(i);
                continue;
            }
            
            /* поиск пробелов */
            matcher = WHITESPACES_PATTERN.matcher(line);
            /* индекс прошлого совпадения */
            int lastGroupIndex = 0;
            while(matcher.find()) {
                /* индекс совпадения с шаблоном */
                int groupIndex = line.indexOf(matcher.group(), lastGroupIndex);
                lastGroupIndex = groupIndex + 1;
                
                /* подстрока, идущая перед найденным совпадением
                 * необходимо */
                String previousString = line.substring(Math.max(0, groupIndex - 8), groupIndex);
                /* если совпадение находится в самом начале строки
                 * то это отступы, указывающие на вложенность блоков кода
                 * если совпадение найдено после ключевого слова или после метасимволов,
                 * используемых в указании типа переменной 
                 * 
                 * то найденное совпадение удалять нельзя, но его можно сократить до 1 пробела
                 */
                if(line.startsWith(matcher.group()) || previousString.matches("(" + ALL_KEYWORDS_REGEX + "|" + METASYMBOLS_REGEX + ")$")) {
                    line = line.replaceFirst(matcher.group(), " ");
                }
            }
            
            /* поиск операторов, окруженных пробелами */
            matcher = OPERATORS_PATTERN.matcher(line);
            while(matcher.find()) {
                //удаляем пробелы вокруг операторов
                line = line.replace(matcher.group(), matcher.group().trim());
            }
            
            /* обновляем строку в списке */
            lines.set(i, line);
            /* шагаем к следующей строке */
            i++;
        }
    }
    
    /**
     * Переименовывает все переменные 
     */
    public void renameVariables() {
        HashSet<String> variablesSet = new HashSet<String>();
        
        Matcher matcher;
        for(int i = 1; i < lines.size(); i++) {
            String line = lines.get(i);
            matcher = VARIABLE_DEFINITION_PATTERN.matcher(line);
            while(matcher.find() && !matcher.group().matches(ALL_KEYWORDS_REGEX)) {
               variablesSet.add(matcher.group());
            }
        }
        
        String[] variables = variablesSet.toArray(new String[]{});
        Arrays.sort(variables, new Comparator<String>() {
            public int compare(String a, String b) {
                int al = a.length();
                int bl = b.length();
                
                if(al == bl) return 0;
                if(al > bl) return -1;
                return 1;
            }
        });
        
        for(String var : variables) {
            String newName = randomName();
            for(int i = 0; i < lines.size(); i++) {
                String line = lines.get(i);
                line = line.replaceAll("\\b(?<=[^.])" + var + "(?<=[^.( ])", newName);
                lines.set(i, line);
            }
        }
    }
    
    private char[] seed = { 'a', 'a', 'a', 'a', 'a' };
    
    /**
     * Возвращает новое случайное имя
     */
    private String randomName() {
        //берем следующую буковку, чтобы не повторяться
        seed[4]++;
        for(int i = 4; i > 0; i--) {
            //проверяем, чтобы не было кракозябр
            if(seed[i] > 'z') {
                //нашли кракозябру
                seed[i] = 'a';
                seed[i - 1]++;
            } else {
                //кракозябр нет, мой генералъ
                break;
            }
        }
        //соединяем все отдельные буковки в одну строку
        String name = new String(seed);
        //самое веселое
        /*
         * формируем новое имя в формате:
         * _0х<случайное число от 0 до 192>
         * <три первых буковки из массива>
         * <случайное число от 0 до 128>
         * <две последние буковки из массива>
         */
        return "_0x" + (int)(Math.random() * 192) + 
               seed[0] + seed[1] + seed[2] + 
               (int)(Math.random() * 128) + 
               seed[3] + seed[4];
    }
    
    @Override
    public String toString() {
        StringBuilder joiner = new StringBuilder();
        for(String line : lines) {
            joiner.append(line).append("\n");
        }
        String result = joiner.toString();
        
        Matcher matcher = STRING_LITERAL_ID_PATTERN.matcher(result);
        while(matcher.find()) {
            int id = Integer.parseInt(matcher.group().substring(1));
            result = result.replaceAll(matcher.group() + "\\b", stringLiterals.get(id).replace("$", "\\$"));
        }
        
        return result;
    }
}
