package org.kllbff.jcry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class JSource {
    /**
     * Массив 50 ключевых cлов Java SE 8
     */
    public static final String[] KEYWORDS = {
        "byte", "short", "int", "long", "char", "float", "double", "boolean",
        "if", "else", "switch", "case", "default", "while", "do", "break", "continue", "for",
        "try", "catch", "finally", "throw", "throws", 
        "private", "protected", "public",
        "import", "package", "class", "interface", "extends", "implements", "static", "final", "void", "abstract", "native",
        "new", "return", "this", "super",
        "synchronized", "volatile",
        "const", "goto", "enum", "instanceof", "assert", "transient", "strictfp"
    };
    public static final String ALl_KEYWORDS_REGEX;

    static {
        StringJoiner joiner = new StringJoiner("|");
        for(String keyword : KEYWORDS) {
            joiner.add(keyword);
        }
        ALl_KEYWORDS_REGEX = joiner.toString();
    }

    /**
     * Удаляет комментарии из исходного кода, представленного строкой source
     * 
     * @warning использование метода может затронуть содержимое строковых констант
     * @param source исходный код
     * @return исходный код за исключением комментариев
     */
    public static String deleteComments(String source) {
        source = source.replaceAll("/\\*(.|\n|\t|\r)*\\*/|//.*\n", "");
        return source;
    }
    
    public JSource() {
        JSource.this.isKeyWord(null);
    }
    
    public JSource(String test/*comment*/) {//test
        //do something
    }

    /**
     * Обфусцирует код, удаляя из него переходы на новую строчку. Таким образом, код будет записан в одну единую длиную строчку
     * 
     * @warning использование метода может затронуть содержимое строковых констант
     * @param source исходный код
     * @return обфусцированный код
     */
    public static String blindToOneLine(String source) {
        source = source.replaceAll("(\n|\r)*", "");
        return source;
    }

    /**
     * Удаляет пробелы из исходного кода
     * 
     * @warning использование метода может затронуть содержимое строковых констант
     * @param source исходный код
     * @return код с удалёнными пробелами (2 и более)
     */
    public static String deleteWhitespaces(String source) {
        source = source.replaceAll(" +", " ");
        return source;
    }

    /**
     * Проверяет принадлежность идентификатора id к числу ключевых слов
     * 
     * @param id идентификатор
     * @return true огда и только тогда, если id - одно из клчевых слов
     */
    public static boolean isKeyWord(String id) {
        return id.matches(ALl_KEYWORDS_REGEX);
    }

    /**
     * Исходный код 
     */
    private String source;

    /**
     * Позволяет задать объекту новый исходный код для его дальнейшей обработки
     * 
     * @param source исходный код
     */
    public void setSource(CharSequence source) {
        this.source = source.toString();
    }

    /**
     * Возвращает обфусцированный код
     * 
     * @return обфусцированный код
     */
    public CharSequence obfuscate() {
        StringBuilder produced = new StringBuilder();
        HashMap<Integer, String> map = getStringConstants();
        Integer[] indexes = map.keySet().toArray(new Integer[0]);
        Arrays.sort(indexes);
        
        int lastIndex = 0, index;
        String currentPart;
        for(int i = 0; i <= indexes.length; i++) {
            if(i == indexes.length)
                index = source.length();
            else
                index = indexes[i];
            
            currentPart = source.substring(lastIndex, index);
            currentPart = deleteWhitespaces(currentPart);
            currentPart = deleteComments(currentPart);
            currentPart = blindToOneLine(currentPart);
            produced.append(currentPart);
            
            if(i != indexes.length) {
                currentPart = map.get(index);
                produced.append(currentPart);
            }
            
            lastIndex = index + currentPart.length();
        }
        
        return produced;
    }

    /**
     * Возвращает таблицу строковых констант, в которой в качестве ключа 
     * выступает индекс открывающей кавычки константы в исходном коде, а 
     * значением - сама константа
     * 
     * @return таблица строковых констант
     */
    public HashMap<Integer, String> getStringConstants() {
        HashMap<Integer, String> map = new HashMap<>();
        int lastIndex = -1;
        for(int i = 0; i < source.length(); i++) {
            if(source.charAt(i) != '\"' || (i > 0 && source.charAt(i - 1) == '\\'))
                continue;
            
            if(lastIndex != -1) {
                map.put(lastIndex, source.substring(lastIndex, i + 1));
                lastIndex = -1;
            } else {
                lastIndex = i;
            }
        }
        
        return map;
    }

    /**
     * Возвразает список идентфикаторов
     * 
     * @param ignoreKeyWords позволяет игнорировать ключевые слова
     * @param ignoreFounded позволяет игнорировать уже найденные идентификаторы (дубликаты)
     * @return список идентификаторов
     */
    public ArrayList<String> getIdentifiers(boolean ignoreKeyWords, boolean ignoreFounded) {
        ArrayList<String> identifiers = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\b[A-Za-zА-Яа-я_]\\w*\\b");
        Matcher matcher = pattern.matcher(source);
        
        String id;
        while(matcher.find()) {
            id = matcher.group();
            if((ignoreKeyWords && isKeyWord(id)) || 
               (ignoreFounded && identifiers.contains(id)))
                continue;
                
            identifiers.add(id);
        }
        
        return identifiers;
    }
}
